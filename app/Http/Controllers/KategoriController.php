<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class KategoriController extends Controller
{
    public function create()
    {
        return view ('/kategori/create');
    }

    public function store(Request $request)
    {
        $request = $request->validate(
        [
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required',
        ],
        [
            'nama.required' => 'Nama Harus di Isi',
            'umur.required'  => 'Berapa Usia Anda',
            'bio.required'  => 'Bio Harus di isi',
        ]);
        // dd($request->all());
        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
            ]
        );

        return redirect('/');
    
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
 
        return view('cast.index', compact('cast'));

    }



}
