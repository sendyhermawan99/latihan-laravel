@extends('layout.master')
@section('judul')
Halaman Edi Cast    
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label for="exampleInputEmail1">Nama</label>
        <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
        @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputEmail1">Umur</label>
        <input type="text" class="form-control" name="umur" value="{{$cast->umur}}">
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Bio</label>
        <input type="text" class="form-control" name="bio" value="{{$cast->bio}}">
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    </div>
        <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection