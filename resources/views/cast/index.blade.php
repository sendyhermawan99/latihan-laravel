@extends('layout.master')
@section('judul')
Halaman Data Table    
@endsection

@section('content')

<a href="cast/create" class="btn btn-primary my-3">Tambah Cast</a>

<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">#</th>
        <th scope="col">nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Detail</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key => $item)
        <tr>
          <td>{{ $key +1 }}</td>
          <td>{{ $item->nama }}</td>
          <td>{{ $item->umur }}</td>
          <td>{{ $item->bio }}</td>
          <td>
            <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Detail</a>
            <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
            <form action="cast/{{$item->id}}" method="POST">
              @csrf
              @method('delete')
              <input type="submit" class="btn btn-danger btn-sm" value="Delete">
            </form>
          </td>
        </tr>
          
      @empty
          
      @endforelse
    </tbody>
  </table>
  
@endsection