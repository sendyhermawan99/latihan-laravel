@extends('layout.master')
@section('judul')
Halaman Pendaftaran    
@endsection
@section('content')

<form action="/welcome" method="POST">
    @csrf
    <table border="1">
        <tr>
            <td>
                <label>First Name</label>
            </td>
            <td>:</td>
            <td>
                <input type="text" name="firstname" autofocus>
            </td>
        </tr>
        <tr>
            <td>
                <label>Last Name</label>
            </td>
            <td>:</td>
            <td>
                <input type="text" name="lastname">
            </td>
        </tr>
        <tr>
            <td>
                <label>Gender</label>
            </td>
            <td>:</td>
            <td>
                <input type="radio" name="male">Male
                <input type="radio" name="female">Female
            </td>
        </tr>
        <tr>
            <td>
                <label>Nationality</label>
            </td>
            <td>:</td>
            <td>
                <select name="nationality">
                    <option value="">---None--</option>
                    <option value="indonesia">Indonesia</option>
                    <option value="singapure">Singapure</option>
                    <option value="malaysia">Malaysia</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <label>Language Spoken</label>
            </td>
            <td>:</td>
            <td>
                <input type="checkbox" name="indo">Bahasa Indonesia
                <input type="checkbox" name="english">English
                <input type="checkbox" name="other">Other
            </td>
        </tr>
        <tr>
            <td>
                <label>Bio</label><br><br>
            </td>
            <td>:</td>
            <td>
                <textarea name="bio" cols="30" rows="5"></textarea>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <input type="submit" value="Sign Up">
            </td>
        </tr>
    </table>
</form>

@endsection