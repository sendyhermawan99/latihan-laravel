<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;

Route::get('/data-tables', function (){
    return view ('halaman.data-table');
});

Route::get('/table', function (){
    return view ('halaman.table');
});

Route::get('/', 'HomeController@index');
Route::get('/register', 'AuthController@index');
Route::post('/welcome', 'AuthController@welcome');



// CRUD KATEGORI

Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{id}', 'CastController@show');
route::get('cast/{id}/edit', 'CastController@edit');
route::put('cast/{id}', 'CastController@update');
route::delete('cast/{id}', 'CastController@destroy');













